//
//  DisplayAirPort.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 22/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

struct DisplayAirport{
    var name: String?
    var city: String?
    var location: Location?
}

struct Location{
    var longitude: Float?
    var latitude: Float?
}

//
//  DashboardInteractor.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

protocol DashboardBusinessLogic{
    func getAirPorts(request: Dashboard.GetAirports.Request)
    func setDisplayAirPorts(displayAirPorts: [DisplayAirport]?)
    func setLocation(length: Float, latitude: Float)
}

protocol DashboardDataStore{
    var radio: Float? { get set }
    var length: Float? {get set}
    var latitude: Float? {get set}
    var displayPorts: [DisplayAirport]?{ get set}
}

class DashboardInteractor: DashboardBusinessLogic, DashboardDataStore{
   
    var presenter: DashboardPresentationLogic?
    var worker: DashboardWorker = DashboardWorker()
    
    var radio: Float?
    var length: Float?
    var latitude: Float?
    var displayPorts: [DisplayAirport]?
    
    func getAirPorts(request: Dashboard.GetAirports.Request) {
        
        guard let radius = request.radius, let lng = request.length, let lat = request.latitude else{
            return
        }
        
        self.worker.getAirports(radius: radius, length: lng, latitude: lat) { (response, error) in
            
            guard let response = response, let _ = response.airports else{
                self.presenter?.presentError(error: error)
                return
            }
            
            self.radio = radius
            self.length = lng
            self.latitude = lat
            
            self.presenter?.presentAirports(response: response)
        }
        
    }
    
    func setDisplayAirPorts(displayAirPorts: [DisplayAirport]?) {
        self.displayPorts = displayAirPorts
    }
    
    func setLocation(length: Float, latitude: Float) {
        self.length  = length
        self.latitude = latitude
    }
    
    
}

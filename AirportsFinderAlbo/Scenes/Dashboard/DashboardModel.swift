//
//  DashboardModel.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

enum Dashboard
{
  // MARK: Use cases
  
  enum GetAirports
  {
    struct Request
    {
        var radius: Float?
        var length: Float?
        var latitude: Float?
    }
    struct Response
    {
        struct  Airport: Codable {
            var airportId: String?
            var code: String?
            var name: String?
            var location: Location?
            var cityId: String?
            var city: String?
            var countryCode: String?
            var themes: [String]?
            var pointsOfSale: [String]?
        }
        
        struct Location: Codable {
            var longitude: Float?
            var latitude: Float?
        }
        
        var airports: [Airport]?
    }
    struct ViewModel
    {
        var diplayAirPorts: [DisplayAirport]?
    }
  }
}

//
//  DashboardPresenter.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

protocol DashboardPresentationLogic{
    func presentAirports(response: Dashboard.GetAirports.Response)
    func presentError(error: Error?)
}

class DashboardPresenter: DashboardPresentationLogic{
    
    var viewController: DashboardDisplayLogic?
    
    // MARK: Do something
    
    func presentAirports(response: Dashboard.GetAirports.Response){
        var viewModel = Dashboard.GetAirports.ViewModel()
        viewModel.diplayAirPorts = [DisplayAirport]()
        
        guard let airports = response.airports else{
            return
        }
        
        for airport in airports{
            
            let displayAirport = DisplayAirport(name: airport.name,
                                                city: airport.city,
                                                location: Location(longitude: airport.location?.longitude,
                                                                   latitude: airport.location?.latitude))
            
            viewModel.diplayAirPorts?.append(displayAirport)
        }
        
        self.viewController?.displayAirports(viewModel: viewModel)
        
    }
    
    func presentError(error: Error?) {
        let messge = error?.localizedDescription ?? ""
        
        self.viewController?.displayError(message: messge)
    }
}


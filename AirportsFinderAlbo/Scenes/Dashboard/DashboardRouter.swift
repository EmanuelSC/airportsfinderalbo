//
//  DashboardRouter.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

@objc protocol DashboardRoutingLogic{
    func routeToViewController(viewController: UIViewController?)
}

protocol DashboardDataPassing{
    var dataStore: DashboardDataStore? { get }
}

class DashboardRouter: NSObject, DashboardRoutingLogic, DashboardDataPassing
{
    weak var viewController: DashboardViewController?
    var dataStore: DashboardDataStore?
    var displayPorts: [DisplayAirport]?
    
    // MARK: Routing
    
    func routeToViewController(viewController: UIViewController?) {
        
        switch viewController {
        case let map as MapAirPortsViewController :
            var destinatioDS = map.router!.dataStore!
            self.passDataToMapAirPorts(source: dataStore!, destination: &destinatioDS)
            map.interactor?.showDisplayAirPorts()
        case let list as ListAirPortsTableViewController:
            var destinatioDS = list.router!.dataStore!
            self.passDataToListAirPorts(source: dataStore!, destination: &destinatioDS)
            list.interactor?.showDisplayAirPorts()
        default:
            break
        }
        
    }
    
    // MARK: Passing data
    
    func passDataToMapAirPorts(source: DashboardDataStore, destination: inout MapAirPortsDataStore){
        destination.radio = source.radio
        destination.length = source.length
        destination.latitude = source.latitude
        destination.displayPorts = source.displayPorts
    }
    
    func passDataToListAirPorts(source: DashboardDataStore, destination: inout ListAirPortsDataStore){
        destination.displayPorts = source.displayPorts
    }
}

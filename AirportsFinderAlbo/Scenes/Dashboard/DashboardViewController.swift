//
//  DashboardViewController.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit
import CoreLocation

protocol DashboardDisplayLogic: class
{
    func displayAirports(viewModel: Dashboard.GetAirports.ViewModel)
    func displayError(message: String)
}

class DashboardViewController: UITabBarController {
    
    var interactor: DashboardBusinessLogic?
    var router: (NSObjectProtocol & DashboardRoutingLogic & DashboardDataPassing)?
    
    var locationManager: CLLocationManager!

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = DashboardInteractor()
        let presenter = DashboardPresenter()
        let router = DashboardRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureLocation()
        self.delegate = self
    }
    
    func configureLocation(){
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
}

extension DashboardViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let location = locations.last! as CLLocation
        
        let request = Dashboard.GetAirports.Request(radius: self.router?.dataStore?.radio,
                                                    length: Float(location.coordinate.longitude),
                                                    latitude: Float(location.coordinate.latitude))
        
        self.locationManager.stopUpdatingLocation()
        
        self.interactor?.getAirPorts(request: request)
    }
}

extension DashboardViewController: UITabBarControllerDelegate{

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let tabViewControllers = tabBarController.viewControllers!
        guard let toIndex = tabViewControllers.firstIndex(of: viewController) else {
            return false
        }

        self.router!.routeToViewController(viewController: viewController)

        self.animateToTab(toIndex: toIndex)

        return true

    }
    
    func animateToTab(toIndex index: Int) {
        let tabViewControllers = viewControllers!
        let fromView = selectedViewController!.view
        let toView = tabViewControllers[index].view
        let fromIndex = tabViewControllers.firstIndex(of: selectedViewController!)
        
        guard fromIndex != index else{
            return
        }
        
        fromView!.superview!.addSubview(toView!)
        
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width;
        let scrollRight = index > fromIndex!;
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView?.center = CGPoint(x: (fromView?.center.x)! + offset, y: (toView?.center.y)!)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            
            // Slide the views by -offset
            fromView!.center = CGPoint(x: (fromView?.center.x)! - offset, y: (fromView?.center.y)!);
            toView?.center   = CGPoint(x: (toView?.center.x)! - offset, y: (toView?.center.y)!);
            
        }, completion: { finished in
            
            // Remove the old view from the tabbar view.
            fromView?.removeFromSuperview()
            self.selectedIndex = index
            self.view.isUserInteractionEnabled = true
        })
    }
}


extension DashboardViewController: DashboardDisplayLogic{
    func displayAirports(viewModel: Dashboard.GetAirports.ViewModel) {
        self.interactor?.setDisplayAirPorts(displayAirPorts: viewModel.diplayAirPorts)
        
        let vc = self.selectedViewController
        self.router!.routeToViewController(viewController: vc)
    }
    
    func displayError(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        self.present(alert, animated: true)
    }
}

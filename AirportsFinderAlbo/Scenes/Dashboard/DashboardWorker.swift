//
//  DashboardWorker.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

typealias handler = (Dashboard.GetAirports.Response?, Error?) -> ()

class DashboardWorker
{
    func getAirports(radius: Float, length: Float, latitude: Float, completitionHandler: @escaping handler)
    {
        let headers = [
            "x-rapidapi-host": "cometari-airportsfinder-v1.p.rapidapi.com",
            "x-rapidapi-key": "c3b58fc53emsh065bb23854d3aedp1a14c3jsn604b43a89057"
        ]

        let url = URL(string: "https://cometari-airportsfinder-v1.p.rapidapi.com/api/airports/by-radius?radius=\(radius)&lng=\(length)&lat=\(latitude)")

        var request = URLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request , completionHandler:{ (data, response, error) -> Void in

            guard let data = data, error == nil else{
                DispatchQueue.main.async {
                    completitionHandler(nil, error)
                }
                return
            }
            
            do{
                let decoder = JSONDecoder()
                
                typealias response = [Dashboard.GetAirports.Response.Airport]
                
                let airports = try decoder.decode(response.self, from: data)
                
                var result = Dashboard.GetAirports.Response()
                result.airports = airports
                
                DispatchQueue.main.async {
                    completitionHandler(result, nil)
                }
                
            }catch{
                DispatchQueue.main.async {
                    completitionHandler(nil, error)
                }
            }
        })

        dataTask.resume()
    }
}

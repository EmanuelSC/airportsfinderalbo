//
//  IntroductionInteractor.swift
//  AirportsFinderAlboSwiftUI
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

protocol IntroductionBusinessLogic
{
  //func doSomething(request: Introduction.Something.Request)
}

protocol IntroductionDataStore
{
  var radio: Float? { get set }
}

class IntroductionInteractor: IntroductionBusinessLogic, IntroductionDataStore
{
    var presenter: IntroductionPresentationLogic?
    var worker: IntroductionWorker?
    
    var radio: Float?
    
}

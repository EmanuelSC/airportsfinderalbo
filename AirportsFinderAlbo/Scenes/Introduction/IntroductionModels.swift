//
//  IntroductionModels.swift
//  AirportsFinderAlboSwiftUI
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

enum Introduction
{
  // MARK: Use cases
  
  enum Something
  {
    struct Request
    {
    }
    struct Response
    {
    }
    struct ViewModel
    {
    }
  }
}

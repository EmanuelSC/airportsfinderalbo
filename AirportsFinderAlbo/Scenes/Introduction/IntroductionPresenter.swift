//
//  IntroductionPresenter.swift
//  AirportsFinderAlboSwiftUI
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

protocol IntroductionPresentationLogic
{
  func presentSomething(response: Introduction.Something.Response)
}

class IntroductionPresenter: IntroductionPresentationLogic
{
  var viewController: IntroductionDisplayLogic?
  
  // MARK: Do something
  
  func presentSomething(response: Introduction.Something.Response)
  {
//    let viewModel = Introduction.Something.ViewModel()
//    viewController?.displaySomething(viewModel: viewModel)
  }
}

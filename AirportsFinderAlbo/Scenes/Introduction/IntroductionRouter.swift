//
//  IntroductionRouter.swift
//  AirportsFinderAlboSwiftUI
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

@objc protocol IntroductionRoutingLogic{
  func routeToDashboard(segue: UIStoryboardSegue?)
}

protocol IntroductionDataPassing{
  var dataStore: IntroductionDataStore? { get set}
}

class IntroductionRouter: NSObject, IntroductionRoutingLogic, IntroductionDataPassing
{
  weak var viewController: IntroductionViewController?
  var dataStore: IntroductionDataStore?
  
  // MARK: Routing
  
  func routeToDashboard(segue: UIStoryboardSegue?)
  {
    if let segue = segue {
        let destinationVC = segue.destination as! DashboardViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToDashboard(source: dataStore!, destination: &destinationDS)
    } else {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToDashboard(source: dataStore!, destination: &destinationDS)
        navigateToDashboard(source: viewController!, destination: destinationVC)
    }
  }

  // MARK: Navigation
  
  func navigateToDashboard(source: IntroductionViewController, destination: DashboardViewController)
  {
    source.show(destination, sender: nil)
  }
  
  // MARK: Passing data
  
  func passDataToDashboard(source: IntroductionDataStore, destination: inout DashboardDataStore)
  {
    destination.radio = source.radio
  }
}

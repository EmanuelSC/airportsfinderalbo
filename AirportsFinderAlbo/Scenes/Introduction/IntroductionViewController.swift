//
//  IntroductionViewController.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

protocol IntroductionDisplayLogic: class
{

}

class IntroductionViewController: UIViewController {

    @IBOutlet weak var lblSliderValue: UILabel!
    @IBOutlet weak var sliderRadius: UISlider!
    @IBOutlet weak var btnSearch: UIButton!
    
    var interactor: IntroductionBusinessLogic?
    var router: (NSObjectProtocol & IntroductionRoutingLogic & IntroductionDataPassing)?

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = IntroductionInteractor()
        let presenter = IntroductionPresenter()
        let router = IntroductionRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureIntroductionViewController()
    }
    
    
    private func configureIntroductionViewController(){
        
        self.lblSliderValue.text = "\(Int(self.sliderRadius.value))"
        self.router?.dataStore?.radio = self.sliderRadius.value
        
        self.sliderRadius.addTarget(self,
                                    action: #selector(sliderEditingChanged(sender:)),
                                    for: .valueChanged)
        
        self.btnSearch.layer.cornerRadius = 10
    }
    
    @objc private func sliderEditingChanged(sender: UISlider){
        self.lblSliderValue.text = "\(Int(sender.value))"
        self.router?.dataStore?.radio = sender.value
    }
    
    @objc private func pressSearch(sender: UIButton){
        
    }

    
    // MARK: - Routing

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let scene = segue.identifier {
          let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
          if let router = router, router.responds(to: selector) {
            router.perform(selector, with: segue)
          }
        }
    }

}

extension IntroductionViewController: IntroductionDisplayLogic{
    
}

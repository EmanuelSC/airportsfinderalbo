//
//  ListAirPortsInteractor.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

protocol ListAirPortsBusinessLogic{
    func showDisplayAirPorts()
}

protocol ListAirPortsDataStore{
    var displayPorts: [DisplayAirport]?{ get set}
}

class ListAirPortsInteractor: ListAirPortsBusinessLogic, ListAirPortsDataStore{
    
    var presenter: ListAirPortsPresenter?
    var displayPorts: [DisplayAirport]?
    
    func showDisplayAirPorts() {
        
        guard let airports = displayPorts else{
            return
        }
        
        self.presenter?.presentAirports(airports: airports)
    }
    
}

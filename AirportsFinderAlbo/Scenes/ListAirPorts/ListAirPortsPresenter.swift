//
//  ListAirPortsPresenter.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit
import CoreLocation

protocol ListAirPortsPresentationLogic{
    func presentAirports(airports: [DisplayAirport])
}

class ListAirPortsPresenter: ListAirPortsPresentationLogic{
    
    var viewController: ListAirPortsDisplayLogic?
    
    func presentAirports(airports: [DisplayAirport]) {
        self.viewController?.displayAirports(airports: airports)
    }
}

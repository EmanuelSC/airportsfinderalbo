//
//  ListAirPortsRouter.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

@objc protocol ListAirPortsRoutingLogic{
    
}

protocol ListAirPortsDataPassing{
    var dataStore: ListAirPortsDataStore? { get }
}

class ListAirPortsRouter: NSObject, ListAirPortsRoutingLogic, ListAirPortsDataPassing{
    
    weak var viewController: ListAirPortsTableViewController?
    var dataStore: ListAirPortsDataStore?
    
    // MARK: Routing
}

//
//  ListAirPortsTableViewController.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

protocol ListAirPortsDisplayLogic: class{
    func displayAirports(airports: [DisplayAirport])
}


class ListAirPortsTableViewController: UITableViewController {

    var interactor: ListAirPortsBusinessLogic?
    var router: (NSObjectProtocol & ListAirPortsRoutingLogic & ListAirPortsDataPassing)?
    
    var list: [DisplayAirport]?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = ListAirPortsInteractor()
        let presenter = ListAirPortsPresenter()
        let router = ListAirPortsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let list = self.list else{
            return 0
        }
        
        return list.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AirportCell", for: indexPath)

        guard let list = self.list else{
            return cell
        }
        
        let item = list[indexPath.row]
        
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = item.city

        return cell
    }

}

extension ListAirPortsTableViewController: ListAirPortsDisplayLogic{
    func displayAirports(airports: [DisplayAirport]) {
        
        self.list = airports
        self.tableView.reloadData()
    }
}

//
//  MapAirPortsInteractor.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

protocol MapAirPortsBusinessLogic{
    func showDisplayAirPorts()
}

protocol MapAirPortsDataStore{
    var radio: Float? { get set }
    var length: Float? {get set}
    var latitude: Float? {get set}
    var displayPorts: [DisplayAirport]?{ get set}
}

class MapAirPortsInteractor: MapAirPortsBusinessLogic, MapAirPortsDataStore{
    
    var presenter: MapAirPortsPresenter?
    
    var radio: Float?
    var length: Float?
    var latitude: Float?
    var displayPorts: [DisplayAirport]?
    
    func showDisplayAirPorts() {
        
        guard let airports = displayPorts, let radio = self.radio, let lng = self.length, let lat = self.latitude else{
            return
        }
        
        self.presenter?.presentAirports(radio: radio, length: lng, latitude: lat, airports: airports)
    }
    
}

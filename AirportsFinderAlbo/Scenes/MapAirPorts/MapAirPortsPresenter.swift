//
//  MapAirPortsPresenter.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit
import CoreLocation

protocol MapAirPortsPresentationLogic{
    func presentAirports(radio: Float, length: Float, latitude: Float, airports: [DisplayAirport])
}

class MapAirPortsPresenter: MapAirPortsPresentationLogic{
    
    var viewController: MapAirPortsDisplayLogic?
    
    func presentAirports(radio: Float, length: Float, latitude: Float, airports: [DisplayAirport]) {
        
        var airportsAnnotations = [AirportAnnotation]()
        
        airports.forEach { (airport) in
            
            let location = CLLocation(latitude: CLLocationDegrees((airport.location?.latitude)!),
                                      longitude: CLLocationDegrees((airport.location?.longitude)!))
            
            let annotation = AirportAnnotation(title: airport.name!,
                                               locationName: airport.city!,
                                               coordinate: location.coordinate)
            
            airportsAnnotations.append(annotation)
            
        }
        
        
        self.viewController?.displayAirports(radio: radio,
                                             length: length,
                                             latitude: latitude,
                                             airports: airportsAnnotations)
    }
}

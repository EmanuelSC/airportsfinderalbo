//
//  MapAirPortsRouter.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit

@objc protocol MapAirPortsRoutingLogic{
    
}

protocol MapAirPortsDataPassing{
    var dataStore: MapAirPortsDataStore? { get }
}

class MapAirPortsRouter: NSObject, MapAirPortsRoutingLogic, MapAirPortsDataPassing{
    
    weak var viewController: MapAirPortsViewController?
    var dataStore: MapAirPortsDataStore?
    
    // MARK: Routing
}

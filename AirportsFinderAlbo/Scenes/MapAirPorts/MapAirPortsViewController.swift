//
//  MapAirPortsViewController.swift
//  AirportsFinderAlbo
//
//  Created by Emanuel Sánchez on 21/03/20.
//  Copyright © 2020 Emanuel Sánchez. All rights reserved.
//

import UIKit
import MapKit

protocol MapAirPortsDisplayLogic: class{
    func displayAirports(radio: Float, length: Float, latitude: Float, airports: [AirportAnnotation])
}

class MapAirPortsViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    var interactor: MapAirPortsBusinessLogic?
    var router: (NSObjectProtocol & MapAirPortsRoutingLogic & MapAirPortsDataPassing)?

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = MapAirPortsInteractor()
        let presenter = MapAirPortsPresenter()
        let router = MapAirPortsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func centerMapOnLocation(location: CLLocation, regionRadius: CLLocationDistance) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius,
                                                  longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}

extension MapAirPortsViewController: MapAirPortsDisplayLogic{
    func displayAirports(radio: Float, length: Float, latitude: Float, airports: [AirportAnnotation]) {
        let location = CLLocation(latitude: CLLocationDegrees(latitude),
                                  longitude: CLLocationDegrees(length))
        
        let regionRadius = CLLocationDistance(exactly: radio*1000)
        
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius ?? 20,
                                                  longitudinalMeters: regionRadius ?? 20)
        
        mapView.setRegion(coordinateRegion, animated: true)
        
        mapView.addAnnotations(airports)
        
    }
}

//MARK: Annotation Class

class AirportAnnotation: NSObject, MKAnnotation {
  let title: String?
  let locationName: String
  let coordinate: CLLocationCoordinate2D
  
  init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
    self.title = title
    self.locationName = locationName
    self.coordinate = coordinate
    
    super.init()
  }
  
  var subtitle: String? {
    return locationName
  }
}
